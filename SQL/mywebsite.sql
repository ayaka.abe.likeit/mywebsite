﻿CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;

USE mywebsite;

CREATE TABLE delivery_method(
	id int(11) PRIMARY KEY,
	name varchar(255) NOT NULL,
	price int(11) NOT NULL
)


CREATE TABLE item(
	id int PRIMARY KEY,
pr	item_name varchar(255) NOT NULL,
	file_name varchar(255) NOT NULL,
	brand_name varchar(255) NOT NULL,
	sell_price int NOT NULL,
	s_category_id int UNIQUE NOT NULL,
	sale_date date NOT NULL,
	stock int NOT NULL,
	cost_price int NOT NULL,
	coment varchar(255) NOT NULL,
	renewal_date date
)


CREATE TABLE s_category(
	id int PRIMARY KEY,
	s_category_name varchar(255) NOT NULL,
	b_category_id int  NOT NULL
)



CREATE TABLE b_category(
	id int PRIMARY KEY,
	b_category_name varchar(255) NOT NULL
)

CREATE TABLE user(
	id int PRIMARY KEY,
	login_id varchar(255) UNIQUE NOT NULL,
	login_password varchar(255) NOT NULL,
	user_name varchar(255) NOT NULL,
	birth_date date NOT NULL,
	adress varchar(255) NOT NULL,
	create_date datetime NOT NULL,
	update_date datetime NOT NULL
)

CREATE TABLE buy_history(
	id int PRIMARY KEY,
	user_id int NOT NULL,
	total_price int NOT NULL,
	delivery_method_id int NOT NULL,
	buy_date datetime NOT NULL
)


CREATE TABLE buy_detail(
	id int PRIMARY KEY,
	buy_id int NOT NULL,
	item_id int NOT NULL
)

INSERT INTO user VALUES(
	2,
	'nanako',
	'password',
	'佐藤菜々子',
	'2000-12-24',
	'沖縄県那覇市字鏡水150',
	'2019-12-11',
	'2019-12-11'
)


INSERT INTO b_category VALUES
	(1,'スキンケア'),
	(2,'UVケア'),
	(3,'ベースメイク'),
	(4,'メイクアップ'),
	(5,'香水')

INSERT INTO s_category VALUES
	(12,'香水',5)



INSERT INTO item VALUES(
	1,
	'lip0001',
	'アディクトリップマキシマイザー',
	'ディオールマキシマイザー.jpg',
	'Dior',
	3300,
	9,
	'2007-07-27',
	30,
	800,
	'コラーゲン、ヒアルロン酸微粒子を配合したリップケアのできるリップグロス。潤いをアップし、縦ジワの目立たないふっくらとした唇に導きます。',
	NULL
	
ALTER TABLE item CHANGE id id int AUTO_INCREMENT

ALTER TABLE item DROP INDEX s_category_id


INSERT INTO item (item_id, item_name, file_name, brand_name, sell_price,s_category_id, sale_date, stock, cost_price, coment) Values
('lop002', 'リップティント', 'オペラリップティント.jpg', 'オペラ', 1500,  9, '2016-11-01', 25, 600, 'リップケアオイルをベースにしたティント処方のルージュ！さっと1塗りでモテ質感リップに！')

DELETE FROM item WHERE id = 4
SELECT * FROM item WHERE s_category item_id = 'lip0001'

SELECT * FROM item WHERE item_name LIKE '%リップ%' OR brand_name LIKE '%リップ%'

ALTER TABLE buy_detail CHANGE id id int AUTO_INCREMENT

DELETE FROM buy_history WHERE id = 3

UPDATE item SET stock = 30 WHERE id = 1

ALTER TABLE buy_history ADD discount_price int  AFTER total_price
