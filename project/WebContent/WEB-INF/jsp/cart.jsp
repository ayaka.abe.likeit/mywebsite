<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>カート</title>
</head>

<body>

    <!--header-->
    <header>
        <nav class="navbar navbar-dark" style = "background-color: #ffc3c3;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

		 <!--ログインしているとき-->
			<c:if test="${userInfo.name!=null}">
		            <div class="nav-link ml-auto mr-5"><a href="UserBuyListServlet?id=${user.id}">${userInfo.name}さん</a></div>
		           	<a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
		      		<a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
		   	        <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
		    </c:if>

		 <!--ログインしていいないとき-->
		    <c:if test="${userInfo.name == null}">
		            <div class="nav-link ml-auto mr-5">ようこそ！</div>
		             <a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
		              <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
		              <a href ="LoginServlet"><button type="button" class="btn btn-info btn-lg">ログイン</button></a>
		    </c:if>

        </nav>
    </header>


    <div class="container">
        <div class="title-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h3>お買い物かご</h3>
        </div>
        </div>

        <div class="container">
        	<c:if test="${cartMsg != null}">
   				 <div class="alert alert-danger" role="alert">カートには何も入っていません</div>
   			</c:if>

    <!--カートの中身-->
    <form class="form-cart" action="CartServlet" method="post">
    	<c:forEach var="item" items="${cart}">
          <div class="row mt-3">
            <img src="img/${item.fileName}" alt="water"  hspace="50" width="180" height="200">
            <div class="buy">
                    <p>${item.coment}</p>
                <div class="row buyitem-top">
                 <button class="btn-partial-line" type="submit" name="delete" value="${item.id}">削除</button>
                <div class="buyitem-left">
                <h5>価格　${item.sellPrice}円</h5>
                </div>
                </div>
            </div>
        </div>
       </c:forEach>

	       <c:if test="${cartMsg != null}">
	       		現在買い物かごには何も入っておりません。ぜひご利用をお待ちしております。
	       		<p><a href ="HomeServlet">トップページはこちら。</a></p>

	       </c:if>

			<c:if test="${cartMsg == null}">
		        <div class="row justify-content-md-center col-md-auto mt-5">
		            <a href ="BuyServlet"><button type="button" class="btn btn-info btn-lg">購入画面へ進む</button></a>
		        </div>
	        </c:if>

    </form>
    </div>
    </div>

</body>
</html>



