package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyHistoryBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.BuyDetailDao;
import dao.BuyHistoryDao;
import dao.ItemDao;
import dao.UserDao;

/**
 * Servlet implementation class BuyResultServlet
 */
@WebServlet("/BuyResultServlet")
public class BuyResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyResultServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインした人の情報をセッションから取得
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		UserDao userDao =new UserDao();
		UserBeans user = userDao.findbyLoginId(userInfo.getLoginId());

		//取得した情報をセット
		request.setAttribute("user", user);

		//カートセッション
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		//購入した情報
		BuyHistoryBeans buyData = (BuyHistoryBeans)session.getAttribute("buyData");

		//購入した情報をインサート
		BuyHistoryDao buyDao = new BuyHistoryDao();
		int autoNum = buyDao.insertBuy(buyData);

		//購入した情報とユーザー情報紐づけしてインサート
		for (ItemBeans cartItem : cart) {
		int buyId = autoNum;
		int itemId = cartItem.getId();
		BuyDetailDao.insertBuyDetail(buyId, itemId);

		ItemDao itemDao = new ItemDao();
		ItemBeans ib = itemDao.findItem(itemId);
		itemDao.updateStock(ib);
		}

		//カートの中身を削除する
		session.removeAttribute("cart");

	    //購入終了画面
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyResult.jsp");
		dispatcher.forward(request, response);

	}
}
