package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.DBManager;
import beans.ItemBeans;

public class ItemDao {

	//商品新規登録
	public void ItemAdd(String itemId, String itemname, String image, String brand, String price, String category,
			String saleDate,
			String stock, String cost, String coment) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "INSERT INTO item (item_id, item_name, file_name, brand_name, sell_price, "
					+ "s_category_id, sale_date, stock, cost_price, coment) Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, itemId);
			pStmt.setString(2, itemname);
			pStmt.setString(3, image);
			pStmt.setString(4, brand);
			pStmt.setString(5, price);
			pStmt.setString(6, category);
			pStmt.setString(7, saleDate);
			pStmt.setString(8, stock);
			pStmt.setString(9, cost);
			pStmt.setString(10, coment);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	//商品IDが重複していないか確認する
	public ItemBeans findByItemId(String itemId) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM item WHERE item_id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, itemId);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String itemIdData = rs.getString("item_id");
			return new ItemBeans(itemIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//IDに紐づくデータを取り出す
	public ItemBeans findItem(int id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM item WHERE id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			ItemBeans item = new ItemBeans();

			item.setId(rs.getInt("id"));
			item.setItemId(rs.getString("item_id"));
			item.setItemName(rs.getString("item_name"));
			item.setFileName(rs.getString("file_name"));
			item.setBrand(rs.getString("brand_name"));
			item.setSellPrice(rs.getInt("sell_price"));
			item.setsCategoryId(rs.getInt("s_category_id"));
			item.setSaleDate(rs.getDate("sale_date"));
			item.setStock(rs.getInt("stock"));
			item.setCostPrice(rs.getInt("cost_price"));
			item.setComent(rs.getString("coment"));
			item.setRenewalDate(rs.getDate("renewal_date"));

			return item;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

//商品更新
	public void ItemUpdate(String itemName, String price, String renewalDate, String stock, String cost, String coment, String id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "UPDATE item SET item_name = ?, sell_price = ?, renewal_date = ?, stock = ?, cost_price = ?, coment =?  WHERE id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, itemName);
			pStmt.setString(2, price);
			pStmt.setString(3, renewalDate);
			pStmt.setString(4, stock);
			pStmt.setString(5, cost);
			pStmt.setString(6, coment);
			pStmt.setString(7, id);

			pStmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

//商品更新・リニューアル日以外
		public void ItemUpdate(String itemName, String price, String stock, String cost, String coment, String id) {
			Connection conn = null;

			try {
				conn = DBManager.getConnection();
				String sql = "UPDATE item SET item_name = ?, sell_price = ?,  stock = ?, cost_price = ?, coment =?  WHERE id =?";

				PreparedStatement pStmt = conn.prepareStatement(sql);

				pStmt.setString(1, itemName);
				pStmt.setString(2, price);
				pStmt.setString(3, stock);
				pStmt.setString(4, cost);
				pStmt.setString(5, coment);
				pStmt.setString(6, id);

				pStmt.executeUpdate();


			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}

//商品削除
	public void ItemDelete(int id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "DELETE FROM item WHERE id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			pStmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

//商品全件取得
	public List<ItemBeans> ItemfindAll() {
		Connection conn = null;
		List<ItemBeans> itemlist = new ArrayList<ItemBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM item";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String itemId = rs.getString("item_id");
				String itemName = rs.getString("item_name");
				String image = rs.getString("file_name");
				String brand = rs.getString("brand_name");
				int price = rs.getInt("sell_price");
				int category = rs.getInt("s_category_id");
				Date saleDate = rs.getDate("sale_date");
				int stock = rs.getInt("stock");
				int cost = rs.getInt("cost_price");
				String coment = rs.getString("coment");
				Date renewalDate = rs.getDate("renewal_date");

				ItemBeans item = new ItemBeans(id, itemId, itemName, image, brand, price, category, saleDate, stock,
						cost, coment, renewalDate);
				itemlist.add(item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return itemlist;
	}


//商品検索
		public List<ItemBeans> ItemfindSearch(String itemIdP, String itemNameP, String brandP, String categoryP, String saleDate1P, String saleDate2P ) {
			Connection conn = null;
			List<ItemBeans> itemlist = new ArrayList<ItemBeans>();

			try {
				conn = DBManager.getConnection();

				String sql = "SELECT * FROM item WHERE id != 0 ";

				if(!itemIdP.equals("")) {
					 sql += " AND item_id = '" + itemIdP + "'";
				}
				if(!itemNameP.equals("")) {
					 sql += " AND item_name LIKE '%" + itemNameP+ "%'";
				}
				if(!brandP.equals("")) {
					 sql += " AND brand_name LIKE '%" + brandP+ "%'";
				}
				if(!categoryP.equals("")) {
					 sql += " AND s_category_id = " + categoryP ;
				}
				if(!saleDate1P.equals("")) {
					 sql += " AND sale_date >= '" + saleDate1P + "'";
				}

				if(!saleDate2P.equals("")) {
					 sql += " AND  sale_date <= '" + saleDate2P + "'";
				}

				PreparedStatement pStmt = conn.prepareStatement(sql);
				ResultSet rs = pStmt.executeQuery();

				while (rs.next()) {
					int id = rs.getInt("id");
					String itemId = rs.getString("item_id");
					String itemname = rs.getString("item_name");
					String image = rs.getString("file_name");
					String brand = rs.getString("brand_name");
					int price = rs.getInt("sell_price");
					int category = rs.getInt("s_category_id");
					Date saleDate = rs.getDate("sale_date");
					int stock = rs.getInt("stock");
					int cost = rs.getInt("cost_price");
					String coment = rs.getString("coment");
					Date renewalDate = rs.getDate("renewal_date");

					ItemBeans item = new ItemBeans(id, itemId, itemname, image, brand, price, category, saleDate, stock,
							cost, coment, renewalDate);
					itemlist.add(item);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			return itemlist;
		}

//商品検索
		public List<ItemBeans> ItemfindSearch(String  itemP) {
			Connection conn = null;
			List<ItemBeans> itemlist = new ArrayList<ItemBeans>();

			try {
				conn = DBManager.getConnection();

				String sql = "SELECT * FROM item WHERE item_name LIKE '%" + itemP + "%'" ;

						sql += " OR brand_name LIKE '%" + itemP + "%'";

						PreparedStatement pStmt = conn.prepareStatement(sql);
						ResultSet rs = pStmt.executeQuery();

						while (rs.next()) {
							int id = rs.getInt("id");
							String itemId = rs.getString("item_id");
							String itemname = rs.getString("item_name");
							String image = rs.getString("file_name");
							String brand = rs.getString("brand_name");
							int price = rs.getInt("sell_price");
							int category = rs.getInt("s_category_id");
							Date saleDate = rs.getDate("sale_date");
							int stock = rs.getInt("stock");
							int cost = rs.getInt("cost_price");
							String coment = rs.getString("coment");
							Date renewalDate = rs.getDate("renewal_date");

							ItemBeans item = new ItemBeans(id, itemId, itemname, image, brand, price, category, saleDate, stock,
									cost, coment, renewalDate);
							itemlist.add(item);
						}
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
					}
					return itemlist;
				}

		public void updateStock(ItemBeans ib) {
			Connection conn = null;

			try {
				conn = DBManager.getConnection();

				String sql = "UPDATE item SET stock = ? WHERE id = ?" ;

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setInt(1, ib.getStock()-1);
				pStmt.setInt(2, ib.getId());

				pStmt.executeUpdate();

					} catch (SQLException e) {
						e.printStackTrace();
					} finally {
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
							}
						}
					}
				}



}
