package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.BCategoryBeans;
import beans.ItemBeans;
import beans.SCategoryBeans;
import dao.BCategoryDao;
import dao.SCategoryDao;

/**
 * Servlet implementation class CategoryServlet
 */
@WebServlet("/CategoryServlet")
public class CategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CategoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//Bカテゴリーの情報を取り出す
		BCategoryDao  dao = new BCategoryDao();
		List<BCategoryBeans> catelist = dao.BcateAll();

		request.setAttribute("catelist", catelist);

		//URLからIDを取得
		String sid = request.getParameter("id");
		int scateid = Integer.parseInt(sid);

		//小カテゴリーの情報を取り出す
		SCategoryDao sdao = new SCategoryDao();
		SCategoryBeans scate = sdao.findScate(scateid);

		request.setAttribute("scate", scate);

		//URLのIDから商品の情報を取得-itemテーブルではs_category_id-
		List<ItemBeans> scateItemlist = sdao.findItemByscateId(scateid);
		request.setAttribute("scateItemlist", scateItemlist);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/category.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
