<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>購入確認画面</title>
</head>

<body>
   <!--header-->
     <header>
        <nav class="navbar navbar-dark" style = "background-color: #ffc3c3;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>
	            	<div class="nav-link ml-auto mr-5"><a href="UserBuyListServlet?id=${user.id}">${userInfo.name}さん</a></div>
	            	<a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
	                <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
	                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
        </nav>
    </header>

     <div class="container">
        <div class="text-center">
            <div class="result-margin">
                <h2>商品の購入が確定しました。</h2>
            </div>
         </div>

          <div class="container">
            <div class="row  mt-5">
                <div class="col-md-6">
                    <a href="UserBuyListServlet?id=${user.id}"><button type="button" class="btn btn-light btn-block">ユーザ購入履歴へ</button></a>
                </div>
                <div class="col-md-6">
                    <a href="HomeServlet"><button type="button" class="btn btn-info btn-block">ホームへ</button></a>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>


