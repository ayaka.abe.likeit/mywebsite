package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.DBManager;
import beans.ItemBeans;
import beans.SCategoryBeans;

public class SCategoryDao {
	public SCategoryBeans findById(int id) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM s_category WHERE id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			SCategoryBeans cate = new SCategoryBeans();

			cate.setId(rs.getInt("id"));
			cate.setsCategoryName(rs.getString("s_category_name"));

			return cate;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

//
	public SCategoryBeans findScate(int scateid){
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM s_category WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, scateid);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			SCategoryBeans scate = new SCategoryBeans();

			scate.setId(rs.getInt("id"));
		    scate.setsCategoryName(rs.getString("s_category_name"));
		    scate.setbCategoryId(rs.getInt("b_category_id"));

		    return 	scate;


		}catch(SQLException e){
			e.printStackTrace();
        	return null;
		} finally {
			 // データベース切断
	          if (conn != null) {
	          	try {
	               conn.close();
	            } catch (SQLException e) {
	                	e.printStackTrace();

	            }
	     }
		}
	}


//カテゴリー全件取得
	public List<SCategoryBeans> ScateAll(){
		Connection conn = null;
		List<SCategoryBeans> sCatelist = new ArrayList<SCategoryBeans>();

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM s_category";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()){

				int id = rs.getInt("id");
		    	String sCateName= rs.getString("s_category_name");
		    	int bCateId = rs.getInt("b_category_id");

		        //取り出したものを詰めてsCatelistに加える
		        SCategoryBeans scate = new SCategoryBeans(id, sCateName, bCateId);
		        sCatelist.add(scate);
			}


		}catch(SQLException e){
			e.printStackTrace();
        	return null;
		} finally {
			 // データベース切断
	          if (conn != null) {
	          	try {
	               conn.close();
	            } catch (SQLException e) {
	                	e.printStackTrace();

	            }
	     }
		}
		return sCatelist;
	}


//BカテゴリーのIDからSカテゴリーの情報を取り出す
	public List<SCategoryBeans> findByBcateId(int cateid){
		Connection conn = null;
		List<SCategoryBeans> sCatelist = new ArrayList<SCategoryBeans>();

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM s_category WHERE b_category_id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, cateid);

			ResultSet rs = pStmt.executeQuery();

			while(rs.next()){

				int id = rs.getInt("id");
		    	String sCateName= rs.getString("s_category_name");
		    	int bCateId = rs.getInt("b_category_id");

		        //取り出したものを詰めてsCatelistに加える
		        SCategoryBeans scate = new SCategoryBeans(id, sCateName, bCateId);
		        sCatelist.add(scate);
			}


		}catch(SQLException e){
			e.printStackTrace();
        	return null;
		} finally {
			 // データベース切断
	          if (conn != null) {
	          	try {
	               conn.close();
	            } catch (SQLException e) {
	                	e.printStackTrace();

	            }
	     }
		}
		return sCatelist;
	}

//SカテゴリーのIDからアイテムの情報を取り出す
		public List<ItemBeans> findItemByscateId(int cateid){
			Connection conn = null;
			List<ItemBeans> scateItemlist = new ArrayList<ItemBeans>();

			try {
				conn = DBManager.getConnection();
				String sql = "SELECT * FROM item WHERE s_category_id =?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setInt(1, cateid);

				ResultSet rs = pStmt.executeQuery();

					while (rs.next()) {
						int id = rs.getInt("id");
						String itemId = rs.getString("item_id");
						String itemName = rs.getString("item_name");
						String image = rs.getString("file_name");
						String brand = rs.getString("brand_name");
						int price = rs.getInt("sell_price");
						int category = rs.getInt("s_category_id");
						Date saleDate = rs.getDate("sale_date");
						int stock = rs.getInt("stock");
						int cost = rs.getInt("cost_price");
						String coment = rs.getString("coment");
						Date renewalDate = rs.getDate("renewal_date");

						ItemBeans item = new ItemBeans(id, itemId, itemName, image, brand, price, category, saleDate, stock,
								cost, coment, renewalDate);
						scateItemlist.add(item);
					}

			}catch(SQLException e){
				e.printStackTrace();
	        	return null;
			} finally {
				 // データベース切断
		          if (conn != null) {
		          	try {
		               conn.close();
		            } catch (SQLException e) {
		                	e.printStackTrace();

		            }
		     }
			}
			return scateItemlist;
		}
}
