package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class AdminUserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインしていない場合ログイン画面に遷移
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if(userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		//URLからユーザ個人の情報を取得
		String id = request.getParameter("id");
		int userid = Integer.parseInt(id);

		UserDao userdao = new UserDao();
		UserBeans user = userdao.findPerson(userid);

		request.setAttribute("user", user);

		 //管理者の場合
		if(userInfo != null && userInfo.getLoginId().equals("admin")) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminUserUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//一般ユーザーの場合・個人の詳細画面へ
		if(userInfo != null && !(userInfo.getLoginId().equals("admin")) ) {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
		return;
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//ログインセッション取得
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");


		//フォームに入力された情報を引数にしてDaoのメソッドへ
		String password = request.getParameter("password");
		String passwordconfirm = request.getParameter("passwordconfirm");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String adress = request.getParameter("adress");

		String id = request.getParameter("id");
		int userid = Integer.parseInt(id);

		UserDao userdao = new UserDao();


		if ( !(password.equals(passwordconfirm))  || name.isEmpty() || birthDate.isEmpty() || adress.isEmpty()){

			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminUserUpdate.jsp");
			dispatcher.forward(request, response);
			return;

		}else if(password.isEmpty() && passwordconfirm.isEmpty()) {

			userdao.Update(name, birthDate, adress, id);



			 //管理者の場合
			if(userInfo.getLoginId().equals("admin")) {
				response.sendRedirect("UserListServlet");
				return;
			}

			//一般ユーザーの場合・個人の詳細画面へ
			if(!(userInfo.getLoginId().equals("admin")) ) {
				UserBeans user = userdao.findPerson(userid);
				session.setAttribute("userInfo",user);
				response.sendRedirect("UserBuyListServlet");
				return;
			}
		}


		//全てのフォームが入力されているとき
		userdao.Update(password, name, birthDate, adress, id);

		 //管理者の場合
		if(userInfo.getLoginId().equals("admin")) {
			response.sendRedirect("UserListServlet");
			return;
		}

		//一般ユーザーの場合・個人の詳細画面へ
		if(!(userInfo.getLoginId().equals("admin")) ) {
			UserBeans user = userdao.findPerson(userid);
			session.setAttribute("userInfo",user);
			response.sendRedirect("UserBuyListServlet");
			return;
		}

	}

}
