package beans;

import java.util.List;

import dao.SCategoryDao;

public class BCategoryBeans {
	private int id;
	private String bCategoryName;

	public BCategoryBeans() {
	}

	public BCategoryBeans(int id, String bCateName) {
		this.id = id;
		this.bCategoryName = bCateName;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getbCategoryName() {
		return bCategoryName;
	}
	public void setbCategoryName(String bCategoryName) {
		this.bCategoryName = bCategoryName;
	}

	public List<SCategoryBeans> getsCategoryBeans() {
		SCategoryDao dao = new SCategoryDao();
		return dao.findByBcateId(this.id);

	}



}
