<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>ユーザ一覧</title>
</head>

<body>
    <!--header-->
     <header>
        <nav class="navbar navbar-dark" style = "background-color: #98fb98;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

            <div class="nav-link ml-auto mr-5">${userInfo.name}さん</div>
              <a class="mr-5" href="UserListServlet"><img src="${pageContext.request.contextPath}/img/userlist.png" alt="user"></a>
                <a class="mr-5" href="ItemListServlet"><img src="${pageContext.request.contextPath}/img/book.png"  alt="item"></a>
                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
        </nav>
    </header>

    <div class="container">
        <div class="title-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h1>ユーザ一覧</h1>
        </div>
        </div>
    </div>

 <div class="container">
       <div class="justify-content-md-center col-md-auto">
           <form method="post" action="UserListServlet" class="form-horizontal">
            <div class="form-group row  mt-5">
                <label for="loginid" class="col-md-2 col-form-label">ログインID</label>
                <div class="col-md-8">
                <input type="text" class="form-control" id="loginid" name="loginIdP" >
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="Password" class="col-md-2 col-form-label">ユーザ名</label>
                <div class="col-md-8">
                <input type="text" class="form-control" id="password" name="nameP">
                </div>
            </div>

            <div class="form-group row  mt-3">
                <label for="birthdate" class="col-md-2 col-form-label">生年月日</label>

                <div class="row col-md-10">
                    <div class="col-md-4">
                        <input type="date" class="form-control" id="birthdate" name="birthDate1P">
                    </div>

                    <div class="col-md-2 text-center">
                        <h5>～</h5>
                    </div>

                    <div class="col-md-4">
                        <input type="date" class="form-control" id="birthdate" name="birthDate2P">
                    </div>
                </div>
            </div>


          <div class="btn-margin2">
                <button type="submit" class="btn btn-primary">検索</button>
          </div>
		</form>
	</div>


        <table class="table mt-5" id="table">
            <thead>
                <tr>
                <th scope="col">ログインID</th>
                <th scope="col">ユーザ名</th>
                <th scope="col">生年月日</th>
                <th scope="col"></th>
                </tr>
            </thead>
        <tbody>

        	<c:forEach var="user" items="${userlist}">
                <tr>
                  <th scope="row">${user.loginId}</th>
                  <td>${user.name}</td>
                  <td>${user.birthDate}</td>

                  <td> <a href="UserDetailServlet?id=${user.id}"><button type="button" class="btn btn-primary">詳細</button></a>
                        <a href="UserUpdateServlet?id=${user.id}"><button type="button" class="btn btn-success">更新</button></a>
                        <a href="UserDeleteServlet?id=${user.id}"><button type="button" class="btn btn-danger">削除</button></a>
                  </td>
                </tr>

                </c:forEach>
        </tbody>
        </table>
    </div>

</body>
</html>