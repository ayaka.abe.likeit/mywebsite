<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>新規登録画面</title>
</head>

<body>
    <!--header-->
     <header>
        <nav class="navbar navbar-dark" style = "background-color: #ffc3c3;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

		    <!--ログインしているとき-->
			<c:if test="${userInfo.name!=null}">
		            <div class="nav-link ml-auto mr-5"><a href="userbuyList.html">${userInfo.name}さん</a></div>
		             <a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
		             <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
		             <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
		    </c:if>
		 <!--ログインしていいないとき-->
		    <c:if test="${userInfo.name == null}">
		            <div class="nav-link ml-auto mr-5">ようこそ！</div>
		             <a class="mr-5" href="CartServlet"><img src="${pageContext.request.contextPath}/img/shopping-cart-icon-free56.png"  alt="cart"></a>
		             <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
		             <a href ="LoginServlet"><button type="button" class="btn btn-info btn-lg">ログイン</button></a>
		    </c:if>
        </nav>
    </header>

    <div class="container">
        <div class="title-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h1>ユーザ新規登録</h1>
        </div>
        </div>
    </div>

	<c:if test="${errMsg!=null}">
    	<div class="container">
        <div class="alert alert-danger" role="alert">${errMsg}</div>
        </div>
      </c:if>

<div class="container">
<form method="post" action="UserAddServlet" class="form-horizontal">

    <div class="form-group row mt-5">
                <label for="loginId" class="col-md-2 ml-auto col-form-label">ログインID</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="loginId" name="loginId">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="Password" class="col-md-2 ml-auto col-form-label">パスワード</label>
                <div class="col-md-3 mr-auto">
                <input type="password" class="form-control ml-auto" id="password" name="password">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="Passwordconfirm" class="col-md-2 ml-auto col-form-label">パスワード(確認)</label>
                <div class="col-md-3 mr-auto">
                <input type="password" class="form-control ml-auto" id="passwordconfirm" name="passwordconfirm" >
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="name" class="col-md-2 ml-auto col-form-label">ユーザ名</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="name" name="name">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="birthDate" class="col-md-2 ml-auto col-form-label">生年月日</label>
                <div class="col-md-3 mr-auto">
                <input type="date" class="form-control ml-auto" id="birthDate" name="birthDate" >
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="adress" class="col-md-2 ml-auto col-form-label">住所</label>
                <div class="col-md-3 mr-auto">
                <textarea  class="form-control ml-auto" id="adress" name="adress" rows="3"></textarea>
                </div>
            </div>


            <div class="row justify-content-md-center mt-5">
               <button type="submit" class="btn btn-primary">登録</button>
            </div>


                <div class="row justify-content-md-center">
                    <div class="btn-margin">
                        <a href="HomeServlet"><u> 戻る </u></a>
                    </div>
                </div>
</form>
</div>


</body>
</html>
