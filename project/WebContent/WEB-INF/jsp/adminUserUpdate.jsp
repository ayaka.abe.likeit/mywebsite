<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html  lang="ja">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
         <link href="style.css" rel="stylesheet" type="text/css" />
    <title>ユーザ情報更新</title>
	</head>

<body>
     <header>
        <nav class="navbar navbar-dark" style = "background-color: #98fb98;">
          <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

            	<div class="nav-link ml-auto mr-5">${userInfo.name}さん</div>
              	<a class="mr-5" href="UserListServlet"><img src="img/userlist.png" alt="user"></a>
                <a class="mr-5" href="ItemListServlet"><img src="img/book.png"  alt="item"></a>
               	<a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
        </nav>
    </header>


    <div class="container">
        <div class="title-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h1>ユーザ情報更新</h1>
        </div>
        </div>
    </div>

<c:if test="${errMsg != null}">
    <div class="container">
    	<div class="alert alert-danger" role="alert">${errMsg}</div>
    </div>
</c:if>

  <div class="container">
<form method="post" action="UserUpdateServlet" class="form-horizontal">

    <div class="form-group row mt-5">
                <div class="col-md-2 ml-auto col-form-label">ログインID</div>
                <div class="form-control-plaintext col-md-3 mr-auto"><input type="hidden" value="${user.id}" name="id">${user.loginId}</div>
            </div>

            <div class="form-group row mt-3">
                <label for="password" class="col-md-2 ml-auto col-form-label">パスワード</label>
                <div class="col-md-3 mr-auto">
                <input type="password" class="form-control ml-auto" id="password"  name="password">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="passwordconfirm" class="col-md-2 ml-auto col-form-label">パスワード(確認)</label>
                <div class="col-md-3 mr-auto">
                <input type="password" class="form-control ml-auto" id="passwordconfirm" name="passwordconfirm">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="name" class="col-md-2 ml-auto col-form-label">ユーザ名</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="name" name="name" value="${user.name}">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="birthDate" class="col-md-2 ml-auto col-form-label">生年月日</label>
                <div class="col-md-3 mr-auto">
                <input type="date" class="form-control ml-auto" id="birthDate" name="birthDate" value="${user.birthDate}">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="adress" class="col-md-2 ml-auto col-form-label">住所</label>
                <div class="col-md-3 mr-auto">
                <textarea  class="form-control ml-auto" id="adress" name="adress" rows="3">${user.adress}</textarea>
                </div>
            </div>


            <div class="row justify-content-md-center mt-5">
               <button type="submit" class="btn btn-primary">登録</button>
            </div>


                <div class="row justify-content-md-center">
                    <div class="btn-margin">
                        <a href="UserListServlet"><u> 戻る </u></a>
                    </div>

                </div>
  </form>

 </div>


</body>
</html>