<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>商品マスタ新規登録画面</title>
</head>

<body>
    <!--header-->
      <header>
        <nav class="navbar navbar-dark" style = "background-color: #98fb98;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

            <div class="nav-link ml-auto mr-5">${userInfo.name}さん</div>
              <a class="mr-5" href="UserListServlet"><img src="img/userlist.png" alt="user"></a>
                <a class="mr-5" href="ItemListServlet"><img src="img/book.png"  alt="item"></a>
                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
        </nav>
    </header>

    <div class="container">
        <div class="title-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h1>商品マスタ新規登録</h1>
        </div>
        </div>
    </div>

<div class="container">
		<c:if test="${errMsg!=null}">
	   	 	<div class="alert alert-danger" role="alert">${errMsg}</div>
	   	</c:if>


        <form class="form-horizontal" action="ItemAddServlet" method="post">

            <div class="form-group row mt-5">
                <label for="itemId" class="col-md-2 ml-auto col-form-label">商品ID</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="itemId" name="itemId">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="itemName" class="col-md-2 ml-auto col-form-label">商品名</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="itemName" name="itemName">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="image" class="col-md-2 ml-auto col-form-label">画像</label>
                <div class="col-md-3 mr-auto">
                <input type="file" class="form-control ml-auto" id="image" name="image">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="brand" class="col-md-2 ml-auto col-form-label">ブランド名</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="brand" name="brand">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="price" class="col-md-2 ml-auto col-form-label">価格</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="price" name="price">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="category" class="col-md-2 ml-auto col-form-label">カテゴリー</label>
                    <div class="col-md-3 mr-auto">
                        <select name="category" id="category">
                            <option value="">選択してください</option>
                                <optgroup label="スキンケア">
                                    <option value="1" label="化粧水">化粧水</option>
                                    <option value="2" label="美容液">美容液</option>
                                    <option value="3" label="乳液">乳液</option>
                                    <option value="4" label="フェイスパック">フェイスパック</option>
                                </optgroup>
                            <optgroup label="UVケア">
                                <option value="5" label="日焼け止め">日焼け止め</option>
                            </optgroup>
                            <optgroup label="ベースメイク">
                                <option value="6" label="化粧下地">化粧下地</option>
                                <option value="7" label="ファンデーション">ファンデーション</option>
                                <option value="8" label="コンシーラー">コンシーラー</option>
                            </optgroup>
                            <optgroup label="メイクアップ">
                                <option value="9" label="口紅・グロス">口紅・グロス</option>
                                <option value="10" label="チーク">チーク</option>
                                <option value="11" label="アイブロウ">アイブロウ</option>
                            </optgroup>
                            <optgroup label="香水">
                                <option value="12" label="香水">香水</option>
                            </optgroup>
                        </select>
                   </div>
            </div>

            <div class="form-group row mt-3">
                <label for="saleDate" class="col-md-2 ml-auto col-form-label">発売日</label>
                <div class="col-md-3 mr-auto">
                <input type="date" class="form-control ml-auto" id="saleDate" name="saleDate">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="stock" class="col-md-2 ml-auto col-form-label">在庫数</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="stock" name="stock">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="cost" class="col-md-2 ml-auto col-form-label">仕入れ価格</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="cost" name="cost">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="coment" class="col-md-2 ml-auto col-form-label">詳細コメント</label>
                <div class="col-md-3 mr-auto">
                    <textarea  class="form-control ml-auto" id="coment" name="coment" rows="10"></textarea>
                </div>
            </div>

            <div class="row justify-content-md-center mt-5">
            	<button type="submit" class="btn btn-primary">登録</button></a>
            </div>


                <div class="row justify-content-md-center">
                    <div class="btn-margin">
                        <a href="ItemListServlet"><u> 戻る </u></a>
                    </div>
                </div>
        </form>
    </div>


</body>
</html>