package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.BCategoryBeans;

public class BCategoryDao {
	//IDに紐づくデータを取り出す
	public BCategoryBeans findById(int bCategoryId) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM b_category WHERE id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, bCategoryId);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			BCategoryBeans cate = new BCategoryBeans();

			cate.setId(rs.getInt("id"));
			cate.setbCategoryName(rs.getString("b_category_name"));


			return cate;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	//カテゴリー全件取得
		public List<BCategoryBeans> BcateAll(){
			Connection conn = null;
			List<BCategoryBeans> bCatelist = new ArrayList<BCategoryBeans>();

			try {
				conn = DBManager.getConnection();
				String sql = "SELECT * FROM b_category";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				ResultSet rs = pStmt.executeQuery();

				while(rs.next()){

					int id = rs.getInt("id");
			    	String bCateName= rs.getString("b_category_name");
			    	int bCateId = rs.getInt("id");

			        //取り出したものを詰めてsCatelistに加える
			        BCategoryBeans bcate = new BCategoryBeans(id, bCateName);
			        bCatelist.add(bcate);
				}


			}catch(SQLException e){
				e.printStackTrace();
	        	return null;
			} finally {
				 // データベース切断
		          if (conn != null) {
		          	try {
		               conn.close();
		            } catch (SQLException e) {
		                	e.printStackTrace();

		            }
		     }
			}
			return bCatelist;
		}

}
