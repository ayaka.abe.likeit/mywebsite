<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>注文内容詳細</title>
</head>

<body>
    <!--header-->
 		<header>
	        <nav class="navbar navbar-dark" style = "background-color: #ffc3c3;">
	            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

	            <div class="nav-link ml-auto mr-5"><a href="UserBuyListServlet?id=${peron.id}">${userInfo.name}さん</a></div>
	            	 <a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
	                <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
	                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
	        </nav>
       </header>


    <div class="container">
        <div class="text-center">
            <div class="title-margin">
                <h2>注文内容詳細</h2>
            </div>
         </div>
    </div>

    <div class="container">
        <table class="table">
          <thead>
            <tr class="thead-light">
              <th>購入日時</th>
              <th>配送方法</th>
              <th>合計金額</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>${buyhisData.buyFormatDate}</td>
              <td>${buyhisData.deliveryMethodName}</td>

              <c:if test="${buyhisData.discountPrice == 0}">
              	<td>${buyhisData.totalPrice + buyhisData.deliveryMethodPrice}円</td>
              </c:if>

             <c:if test="${buyhisData.discountPrice != 0}">
              	<td>${buyhisData.discountPrice + buyhisData.deliveryMethodPrice}円</td>
              </c:if>

            </tr>
          </tbody>
        </table>
    </div>

    <div class="container">
        <div class="title-margin">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>商品名</th>
              <th>価格</th>
            </tr>
          </thead>
          <tbody>
          <c:forEach var="item" items="${buyItemList}">
            <tr>
              <td>${item.itemName}</td>
              <td>${item.sellPrice}円</td>
            </tr>
           </c:forEach>

           <c:if test="${buyhisData.discountPrice != 0}">
            <tr>
              <td>キャンペーン値引き額</td>
              <td>-${buyhisData.totalPrice - buyhisData.discountPrice}円</td>
            </tr>
            </c:if>

            <tr>
              <td>${buyhisData.deliveryMethodName}</td>
              <td>${buyhisData.deliveryMethodPrice}円</td>
            </tr>
          </tbody>
        </table>
        </div>
    </div>
    </body>
</html>


