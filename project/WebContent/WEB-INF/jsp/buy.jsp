<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>購入画面</title>
</head>

<body>
   <!--header-->
     <header>
        <nav class="navbar navbar-dark" style = "background-color: #ffc3c3;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>
	            	<div class="nav-link ml-auto mr-5"><a href="UserBuyListServlet?id=${user.id}">${userInfo.name}さん</a></div>
	            	<a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
	                <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
	                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
        </nav>
    </header>

<form class="form-buy" id="table" action="BuyServlet" method="post">
    <div class="container">
        <div class="title-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h3>購入内容</h3>
        </div>
        </div>
        <table class="table">
          <thead>
            <tr>
              <th>商品名</th>
              <th>価格</th>
            </tr>
          </thead>
          <tbody>
        <c:forEach var="item" items="${cart}">
            <tr>
              <td>${item.itemName}</td>
              <td>${item.sellPrice}円</td>
            </tr>
        </c:forEach>
          </tbody>
        </table>


        <div class="container">
            <div class="delivery-top row">
                <label for="deliveryName" class="col-md-auto"><h5>配送方法</h5></label>
                <select class="delivery-left" name="deliveryName">
                    <option value="1">通常配送</option>
                    <option value="2">指定配送</option>
                    <option value="3">特急配送</option>
                 </select>
            </div>
        </div>


            <div class="row justify-content-md-center col-md-auto">
                <button type="submit" class="btn btn-info btn-lg mt-3">購入確認画面へ進む</button>
            </div>

    </div>
    </form>

</body>
</html>