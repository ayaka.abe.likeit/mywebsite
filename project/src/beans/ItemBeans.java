package beans;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import dao.SCategoryDao;

public class ItemBeans {
	private int id;
	private String itemId;
	private String itemName;
	private String fileName;
	private String brand;
	private int sellPrice;
	private int sCategoryId;
	private Date saleDate;
	private int stock;
	private int costPrice;
	private String coment;
	private Date renewalDate;


	public ItemBeans(int id, String itemId, String itemName, String fileName, String brand, int sellPrice, int sCategoryId,
			Date saleDate, int stock, int costPrice,String coment, Date renewalDate ) {
		this.id = id;
		this.itemId = itemId;
		this.itemName = itemName;
		this.fileName = fileName;
		this.brand = brand;
		this.sellPrice = sellPrice;
		this.sCategoryId = sCategoryId;
		this.saleDate = saleDate;
		this.stock = stock;
		this.costPrice = costPrice;
		this.coment = coment;
		this.renewalDate = renewalDate;
	}

	public static int getTotalItemPrice(ArrayList<ItemBeans> items) {
		int total = 0;
		for (ItemBeans item : items) {
			total += item.getSellPrice();
		}
		return total;
	}


	public ItemBeans() {
	}


	public ItemBeans(String itemIdData) {
		this.itemId = itemId;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public int getSellPrice() {
		return sellPrice;
	}
	public void setSellPrice(int sellPrice) {
		this.sellPrice = sellPrice;
	}



	public int getsCategoryId() {
		return sCategoryId;
	}

	public SCategoryBeans getsCategory() {
		SCategoryDao dao = new SCategoryDao();
		return dao.findById(this.sCategoryId);
	}

	public void setsCategoryId(int sCategoryId) {
		this.sCategoryId = sCategoryId;
	}


	public Date getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}
	public String getSaleFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(saleDate);
	}


	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public int getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(int costPrice) {
		this.costPrice = costPrice;
	}
	public String getComent() {
		return coment;
	}
	public void setComent(String coment) {
		this.coment = coment;
	}
	public Date getRenewalDate() {
		return renewalDate;
	}
	public void setRenewalDate(Date renewalDate) {
		this.renewalDate = renewalDate;
	}

	public String getFormatrenewalDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(renewalDate);
	}

}


