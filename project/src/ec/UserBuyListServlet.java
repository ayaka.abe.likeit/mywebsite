package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyHistoryBeans;
import beans.UserBeans;
import dao.BuyHistoryDao;
import dao.UserDao;

/**
 * Servlet implementation class UserBuyListServlet
 */
@WebServlet("/UserBuyListServlet")
public class UserBuyListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインした人の情報をセッションから取得
		HttpSession session = request.getSession();

		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");


		UserDao userDao =new UserDao();
		UserBeans user = userDao.findbyLoginId(userInfo.getLoginId());

		//取得した情報をセット
		request.setAttribute("user", user);


		//ユーザーIDを引数にして購入情報を取得する
		int userId = user.getId();
		BuyHistoryDao buyDao = new BuyHistoryDao();
		List<BuyHistoryBeans> buylist = buyDao.getUserBuyData(userId);

		request.setAttribute("buylist", buylist);


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userbuyList.jsp");
		dispatcher.forward(request, response);
	}
}
