package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemListServlet
 */
@WebServlet("/ItemListServlet")
public class ItemListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインしていない場合ログイン画面に遷移
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if(userInfo == null) {
		response.sendRedirect("LoginServlet");
		return;
		}

		if(!(userInfo.getLoginId().equals("admin")) ) {
			response.sendRedirect("HomeServlet");
			return;
		}


		//登録されている商品を全件取得
		ItemDao itemDao = new ItemDao();
		List<ItemBeans> itemlist = itemDao.ItemfindAll();

		request.setAttribute("itemlist", itemlist);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String itemIdP = request.getParameter("itemIdP");
		String itemNameP = request.getParameter("itemNameP");
		String brandP = request.getParameter("brandP");
		String categoryP = request.getParameter("categoryP");
		String saleDate1P= request.getParameter("saleDate1P");
		String saleDate2P= request.getParameter("saleDate2P");

		ItemDao itemDao =new ItemDao();
		List<ItemBeans> itemlist = itemDao.ItemfindSearch(itemIdP, itemNameP, brandP, categoryP, saleDate1P, saleDate2P);

		request.setAttribute("itemlist", itemlist);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemList.jsp");
		dispatcher.forward(request, response);
	}

}
