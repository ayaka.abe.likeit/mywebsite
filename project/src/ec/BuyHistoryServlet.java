package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyHistoryBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.BuyDetailDao;
import dao.UserDao;

/**
 * Servlet implementation class BUyHistoryServlet
 */
@WebServlet("/BuyHistoryServlet")
public class BuyHistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyHistoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインした人の情報をセッションから取得
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		//ログインIDから個人の情報を取得
		String loginId = userInfo.getLoginId();

		UserDao userDao =new UserDao();
		UserBeans user = userDao.findbyLoginId(loginId) ;

		//取得した情報をセット
		request.setAttribute("user", user);


		//URLからbuy_detailテーブルのIDを取得
		int buyId = Integer.parseInt(request.getParameter("buy_id"));

		//購入日時・配送方法・合計金額
		BuyDetailDao buyDetail =  new BuyDetailDao();
		BuyHistoryBeans buyhisData = buyDetail.getUserBuyDetail(buyId);

		request.setAttribute("buyhisData", buyhisData);


		//その日に購入した物を取得
		List<ItemBeans> buyItemList = buyDetail.getItemBuyListByBuyId(buyId);

		request.setAttribute("buyItemList", buyItemList);

		//購入履歴詳細ページへ
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/history.jsp");
		dispatcher.forward(request, response);
	}
}
