package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemAddServlet
 */
@WebServlet("/ItemAddServlet")
public class ItemAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインしていない場合ログイン画面に遷移
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if(userInfo == null) {
		response.sendRedirect("LoginServlet");
		return;
		}

		if(!(userInfo.getLoginId().equals("admin")) ) {
			response.sendRedirect("HomeServlet");
			return;
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemAdd.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String itemId = request.getParameter("itemId");
		String itemName = request.getParameter("itemName");
		String image = request.getParameter("image");
		String brand = request.getParameter("brand");
		String price = request.getParameter("price");
		String category = request.getParameter("category");
		String saleDate = request.getParameter("saleDate");
		String stock = request.getParameter("stock");
		String cost = request.getParameter("cost");
		String coment = request.getParameter("coment");

		ItemDao itemDao = new ItemDao();
		ItemBeans item = itemDao.findByItemId(itemId);

		if(item != null || itemId.isEmpty() || itemName.isEmpty() || image.isEmpty() || brand.isEmpty() || price.isEmpty() || category.isEmpty() ||saleDate.isEmpty()
				|| stock.isEmpty() || cost.isEmpty() || coment.isEmpty()) {

			request.setAttribute("errMsg", "入力された情報は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemAdd.jsp");
			dispatcher.forward(request, response);
			return;

		}
		itemDao.ItemAdd(itemId, itemName, image, brand, price, category, saleDate, stock, cost, coment);
		response.sendRedirect("ItemListServlet");

	}

}
