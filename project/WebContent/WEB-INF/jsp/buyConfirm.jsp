<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>購入確認画面</title>
</head>

	<body>
	   <!--header-->
	     <header>
	        <nav class="navbar navbar-dark" style = "background-color: #ffc3c3;">
	            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>
		            	<div class="nav-link ml-auto mr-5"><a href="UserBuyListServlet?id=${user.id}">${userInfo.name}さん</a></div>
		            	<a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
		                <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
		                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
	        </nav>
	    </header>

		<form class="form-buy" id="table" action="BuyConfirmServlet" method="post">
		    <div class="container">
		        <div class="title-margin">
			        <div class="row justify-content-md-center col-md-auto">
			            <h3>購入内容確認</h3>
			        </div>
		        </div>

		        <table class="table">
		          <thead>
		            <tr>
		              <th>商品名</th>
		              <th>価格</th>
		            </tr>
		          </thead>
		          <tbody>
		           <c:forEach var="item" items="${cart}">
		            <tr>
		              <td>${item.itemName}</td>
		              <td>${item.sellPrice}円</td>
		            </tr>
		            </c:forEach>

		            <tr>
		              <td>小計</td>
		              <td>${buyData.totalPrice}円</td>
		            </tr>

		            <tr>
		              <td>${buyData.deliveryMethodName}</td>
		              <td>${buyData.deliveryMethodPrice}円</td>
		            </tr>
		          </tbody>
		        </table>
		            <div class="text-center">
		            <c:if test="${buyData.discountPrice == 0}">
		                <h5>合計　${buyData.totalPrice+buyData.deliveryMethodPrice}円</h5>
		            </c:if>

		            <c:if test="${buyData.discountPrice != 0}">
		           		<div class="mb-3">3点以上お買い上げ10％オフ適用</div>
		                <h5>合計　${buyData.discountPrice+buyData.deliveryMethodPrice}円</h5>
		            </c:if>

		                    <div class="text-danger">
		                        <p>※購入を確定します</p>
		                    </div>
		                <a href ="BuyResultServlet"><button type="button" class="btn btn-info btn-lg mt-3">購入確定</button></a>
		            </div>
		    </div>
		 </form>

	</body>
</html>