<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html  lang="ja">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
         <link href="style.css" rel="stylesheet" type="text/css" />
    <title>商品マスタ詳細</title>
    </head>

<body>
       <header>
         <nav class="navbar navbar-dark" style = "background-color: #98fb98;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

            	<div class="nav-link ml-auto mr-5">${userInfo.name}さん</div>
              	<a class="mr-5" href="UserListServlet"><img src="img/userlist.png" alt="user"></a>
                <a class="mr-5" href="ItemListServlet"><img src="img/book.png"  alt="item"></a>
               	<a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
        </nav>
    </header>


    <div class="container">
        <div class="title-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h1>商品マスタ詳細</h1>
        </div>
        </div>
    </div>

    <!--画像-->
    <div id="b-box">
        <div class="container">
            <div class="col-md-2 mr-auto">
                <div class="form-control-plaintext"> <img src="img/${item.fileName}"  alt="item" hspace="50" width="250" height="300">
                </div>
            </div>
        </div>
    </div>

    <!--詳細情報-->
    <div id="c-box">
        <div class="row">
            <label for="itemId" class="col-form-label col-md-4">商品ID</label>
            <div class="col-md-auto">
                <div class="form-control-plaintext">${item.itemId}
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <label for="itemname" class="col-form-label col-md-4">商品名</label>
            <div class="col-md-auto">
                <div class="form-control-plaintext">${item.itemName}
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <label for="brand" class="col-form-label col-md-4">ブランド名</label>
            <div class="col-md-auto">
                <div class="form-control-plaintext">${item.brand}
                </div>
            </div>
        </div>

        <div class="row mt-3">
                <label for="price" class="col-form-label col-md-4">価格</label>
                <div class="col-md-auto">
                    <div class="form-control-plaintext">${item.sellPrice}円
                    </div>
                </div>
        </div>

        <div class="row mt-3">
            <label for="brand" class="col-form-label col-md-4">カテゴリー</label>
            <div class="col-md-auto">
                <div class="form-control-plaintext">${item.sCategory.sCategoryName}
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <label for="saleDate" class="col-form-label col-md-4">発売日</label>
            <div class="col-md-auto">
                <div class="form-control-plaintext">${item.saleDate}
                </div>
            </div>
        </div>

		<c:if test="${item.renewalDate != null}">
            <div class="row mt-3">
		       <label for="saleDate" class="col-form-label col-md-4">リニューアル日</label>
		          <div class="col-md-auto">
		             <div class="form-control-plaintext">${item.renewalDate}
		             </div>
		           </div>
       		 </div>
		</c:if>

        <div class="row mt-3">
            <label for="stock" class="col-form-label col-md-4">在庫数</label>
            <div class="col-md-auto">
                <div class="form-control-plaintext">${item.stock}個
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <label for="cost" class="col-form-label col-md-4">仕入れ価格</label>
            <div class="col-md-auto">
                <div class="form-control-plaintext">${item.costPrice}円
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <label for="cost" class="col-form-label col-md-4">商品説明</label>
            <div class="col-md-auto">
                <div class="form-control-plaintext">${item.coment}
                </div>
            </div>
        </div>


       </div>




</body>
</html>