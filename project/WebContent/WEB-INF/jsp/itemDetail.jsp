<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>商品詳細</title>
</head>

<body>

    <!--header-->
    <header>
        <nav class="navbar navbar-dark" style = "background-color: #ffc3c3;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

		 <!--ログインしているとき-->
			<c:if test="${userInfo.name!=null}">
		        <div class="nav-link ml-auto mr-5"><a href="UserBuyListServlet?id=${user.id}">${userInfo.name}さん</a></div>
		            <a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
		            <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
		            <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
		    </c:if>

		 <!--ログインしていいないとき-->
		    <c:if test="${userInfo.name == null}">
		         <div class="nav-link ml-auto mr-5">ようこそ！</div>
		             <a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
		             <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
		             <a href ="LoginServlet"><button type="button" class="btn btn-info btn-lg">ログイン</button></a>
		    </c:if>
        </nav>
    </header>


     <div class="detail-float">
     	<h5>カテゴリー</h5>
	    	<c:forEach var="cate" items="${catelist}">
	    		${cate.bCategoryName}
	    		<ul class="skincare">
			    	 <c:forEach var="scate" items="${cate.sCategoryBeans}">
			    	  		<li class="menu"> <a href="CategoryServlet?id=${scate.id}" class="link">${scate.sCategoryName}</a></li>
			    	 </c:forEach>
	   			</ul>
	    	 </c:forEach>
     </div>

	<div class="top">
		<h4>${item.itemName}/${item.brand}</h4>
	</div>

    <div class="item-float">
        <div class="item-margin">
          	<img src="img/${item.fileName}" alt="water"  hspace="50" height="300">
        </div>
    </div>


    <form class="form-item" action="ItemDetailServlet" method="post">
	    <div class="item-float2">
	        <h5>商品説明</h5>
	        <p>${item.coment}</p>

		<c:if test="${item.stock >= 1}">
	        <div class="row buyitem-top2">
	            <button type="submit" class="btn btn-info" value="${item.id}" name="add">カートに追加</button>
			        <div class="buyitem-left">
			            <h5>価格　${item.sellPrice}円</h5>
			        </div>
	        </div>
	      </c:if>

	    <c:if test="${item.stock <= 0}">
	    	<div class="text-danger">
     		こちらの商品は売り切れです
     		</div>
   		</c:if>

	    </div>
    </form>

</body>
</html>
