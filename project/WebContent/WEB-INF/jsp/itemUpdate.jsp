<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html  lang="ja">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
         <link href="style.css" rel="stylesheet" type="text/css" />
    <title>商品マスタ更新</title>
    </head>

<body>
       <header>
       <nav class="navbar navbar-dark" style = "background-color: #98fb98;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

            <div class="nav-link ml-auto mr-5">${userInfo.name}さん</div>
              <a class="mr-5" href="UserListServlet"><img src="img/userlist.png" alt="user"></a>
                <a class="mr-5" href="ItemListServlet"><img src="img/book.png"  alt="item"></a>
                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
        </nav>
    </header>


    <div class="container">
        <div class="title-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h1>商品マスタ更新</h1>
        </div>
        </div>
    </div>

    <div class="container">
	    <c:if test="${errMsg != null}">
	   		 <div class="alert alert-danger" role="alert">${errMsg}</div>
	    </c:if>


<form method="post" action="ItemUpdateServlet" class="form-horizontal">

    <div class="form-group row mt-5">
                <div class="col-md-2 ml-auto col-form-label">商品ID</div>
                <div class="col-md-3 mr-auto"><input type="hidden" value="${item.id}" name="id">${item.itemId}</div>
            </div>

            <div class="form-group row mt-3">
                <label for="itemname" class="col-md-2 ml-auto col-form-label">商品名</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="itemname" name="itemName" value="${item.itemName}">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="price" class="col-md-2 ml-auto col-form-label">価格</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="price" name="price" value="${item.sellPrice}">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="renewalDate" class="col-md-2 ml-auto col-form-label">リニューアル日</label>
                <div class="col-md-3 mr-auto">
                <input type="date" class="form-control ml-auto" id="renewalDate" name="renewalDate">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="stock" class="col-md-2 ml-auto col-form-label">在庫数</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="stock" name="stock" value="${item.stock}">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="cost" class="col-md-2 ml-auto col-form-label">仕入れ価格</label>
                <div class="col-md-3 mr-auto">
                <input type="text" class="form-control ml-auto" id="cost" name="cost" value="${item.costPrice}">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="coment" class="col-md-2 ml-auto col-form-label">詳細コメント</label>
                <div class="col-md-3 mr-auto">
                    <textarea  class="form-control ml-auto" id="coment" rows="10" name="coment" >${item.coment}</textarea>
                </div>
            </div>


            <div class="row justify-content-md-center mt-5">
              <button type="submit" class="btn btn-primary">登録</button>
            </div>


                <div class="row justify-content-md-center">
                    <div class="btn-margin">
                        <a href="ItemListServlet"><u> 戻る </u></a>
                    </div>
                </div>
</form>
</div>



</body>
</html>