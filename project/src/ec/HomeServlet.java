package ec;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BCategoryBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.BCategoryDao;
import dao.ItemDao;
import dao.UserDao;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインした人の情報をセッションから取得
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		//ログインしていない人用
		if(userInfo == null) {
			BCategoryDao dao = new BCategoryDao();
			List<BCategoryBeans> catelist = dao.BcateAll();

			request.setAttribute("catelist", catelist);

			//商品情報取り出し
			ItemDao itemDao = new ItemDao();
			List<ItemBeans> itemlist = itemDao.ItemfindAll();

			request.setAttribute("itemlist", itemlist);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/home.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//ログインIDから個人の情報を取得
		String loginId = userInfo.getLoginId();

		UserDao userDao =new UserDao();
		UserBeans user = userDao.findbyLoginId(loginId) ;

		//取得した情報をセット
		request.setAttribute("user", user);


		//カテゴリー情報取得
		BCategoryDao dao = new BCategoryDao();
		List<BCategoryBeans> catelist = dao.BcateAll();

		request.setAttribute("catelist", catelist);


		//商品情報取り出し
		ItemDao itemDao = new ItemDao();
		List<ItemBeans> itemlist = itemDao.ItemfindAll();

		request.setAttribute("itemlist", itemlist);

		//ホーム画面表示
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/home.jsp");
		dispatcher.forward(request, response);

		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//任意の言葉で検索
		String itemP = request.getParameter("search");

		ItemDao itemDao = new ItemDao();
		List<ItemBeans> itemlist = itemDao.ItemfindSearch(itemP);

		request.setAttribute("itemlist", itemlist);

		//空欄や登録されていないワードの時
		if(itemlist == null) {
			response.sendRedirect("HomeServlet");
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/home.jsp");
		dispatcher.forward(request, response);
	}

}
