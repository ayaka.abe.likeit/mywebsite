<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>ホーム画面</title>
</head>

<body>
    <!--header-->
    <header>
        <nav class="navbar navbar-dark" style = "background-color: #ffc3c3;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

 		<!--ログインしているとき-->
			<c:if test="${userInfo.name!=null}">
            	<div class="nav-link ml-auto mr-5"><a href="UserBuyListServlet?id=${user.id}">${userInfo.name}さん</a></div>
            	<a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
                <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
    		</c:if>
 		<!--ログインしていいないとき-->
    		<c:if test="${userInfo.name == null}">
            	<div class="nav-link ml-auto mr-5">ようこそ！</div>
             	<a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
                <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
                <a href ="LoginServlet"><button type="button" class="btn btn-info btn-lg">ログイン</button></a>
    		</c:if>
        </nav>
    </header>


    <div class="container">
    	<form method="post" action="HomeServlet" class="form-horizontal">
	         <div class="form-group row mt-5">
	                <input type="text" class="form-control col-md-4 ml-auto" id="name" name="search" >
	                <label for="name" class="col-md-2 mr-auto"><button type="submit" class="btn btn-info">検索</button></label>
	         </div>
	     </form>
    </div>

     <div class="detail-float">
     	<h5>カテゴリー</h5>
	    	<c:forEach var="cate" items="${catelist}">
	    		${cate.bCategoryName}
	    		<ul class="skincare">
			    	 <c:forEach var="scate" items="${cate.sCategoryBeans}">
			    	  		<li class="menu"> <a href="CategoryServlet?id=${scate.id}" class="link">${scate.sCategoryName}</a></li>
			    	 </c:forEach>
	   			</ul>
	    	 </c:forEach>
     </div>

	<div class="top">
		<h4>商品一覧</h4>
	</div>

   <div class="container">
        <div class="row mt-5">
         	<c:forEach var="item" items="${itemlist}">
             	<div class="col-3">
                	<a href="ItemDetailServlet?id=${item.id}"><img src="img/${item.fileName}" alt="item"  height="200"></a>
                 	<p>${item.itemName}</p>
             	</div>
             </c:forEach>
         </div>
    </div>


</body>
</html>