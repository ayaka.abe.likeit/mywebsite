package beans;

public class SCategoryBeans {
	private int id;
	private String sCategoryName;
	private int bCategoryId;


	public SCategoryBeans(int id, String sCategoryName, int bCategoryId) {
		this.id = id;
		this.sCategoryName = sCategoryName;
		this.bCategoryId = bCategoryId;
	}



	public SCategoryBeans() {

	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getsCategoryName() {
		return sCategoryName;
	}
	public void setsCategoryName(String sCategoryName) {
		this.sCategoryName = sCategoryName;
	}
	public int getbCategoryId() {
		return bCategoryId;
	}
	public void setbCategoryId(int bCategoryId) {
		this.bCategoryId = bCategoryId;
	}

}
