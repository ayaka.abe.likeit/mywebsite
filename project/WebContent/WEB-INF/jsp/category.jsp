<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>カテゴリー選択画面</title>
</head>

<body>

    <!--header-->
    <header>
        <nav class="navbar navbar-dark" style = "background-color: #ffc3c3;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

		 <!--ログインしているとき-->
			<c:if test="${userInfo.name!=null}">
		        <div class="nav-link ml-auto mr-5"><a href="UserDetailServlet?id=${user.id}">${userInfo.name}さん</a></div>
		            <a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
		            <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
		            <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
		    </c:if>

		 <!--ログインしていいないとき-->
		    <c:if test="${userInfo.name == null}">
		         <div class="nav-link ml-auto mr-5">ようこそ！</div>
		             <a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
		             <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
		             <a href ="LoginServlet"><button type="button" class="btn btn-info btn-lg">ログイン</button></a>
		    </c:if>
        </nav>
    </header>


     <div class="detail-float">
     	<h5>カテゴリー</h5>
	    	<c:forEach var="cate" items="${catelist}">
	    		${cate.bCategoryName}
	    		<ul class="skincare">
			    	 <c:forEach var="scate" items="${cate.sCategoryBeans}">
			    	  		<li class="menu"> <a href="CategoryServlet?id=${scate.id}" class="link">${scate.sCategoryName}</a></li>
			    	 </c:forEach>
	   			</ul>
	    	 </c:forEach>
     </div>
<!--
        <h5>カテゴリー</h5>
        スキンケア
        <ul class="skincare">
            <li class="menu" ><a href="category.html" class="link">化粧水</a></li>
            <li class="menu"><a href="#" class="link">美容液</a></li>
            <li class="menu"><a href="#" class="link">乳液</a></li>
            <li class="menu"><a href="#" class="link">フェイスパック</a></li>
        </ul>

        UVケア
        <ul class="skincare">
            <li class="menu" ><a href="#" class="link">日焼け止め</a></li>
        </ul>

        ベースメイク
        <ul class="skincare">
            <li class="menu" ><a href="#" class="link">化粧下地</a></li>
            <li class="menu"><a href="#" class="link">ファンデーション</a></li>
            <li class="menu"><a href="#" class="link">コンシーラー</a></li>
        </ul>

        メイクアップ
        <ul class="skincare">
            <li class="menu"><a href="#" class="link">口紅・グロス</a></li>
            <li class="menu"><a href="#" class="link">チーク</a></li>
            <li class="menu"><a href="#" class="link">アイブロウ</a></li>
        </ul>

        香水
        <ul class="skincare">
            <li class="menu" ><a href="#" class="link">香水</a></li>
        </ul>
    </div>
-->

<!--
     <div class="main-float">
        <h4>${scate. sCategoryName}</h4>
         <div class="item-margin">
         <c:forEach var="scateItem" items="${scateItemlist}">
         <a href="itemDetail.html"><img src="img/${scateItem.fileName}" alt="water"  hspace="50" width="180" height="200"></a>
         <p>
         ${scateItem.itemName}
         </p>
         </c:forEach>
         </div>
    </div>
   -->

     <div class="container top">
        <h4>${scate. sCategoryName}</h4>
         <div class="row">
         	<c:forEach var="scateItem" items="${scateItemlist}">
             	<div class="col-4">
                	<a href="ItemDetailServlet?id=${scateItem.id}"><img src="img/${scateItem.fileName}" alt="item"  height="200"></a>
                 	<p>${scateItem.itemName}</p>
             	</div>
             </c:forEach>
         </div>
    </div>

</body>
</html>

