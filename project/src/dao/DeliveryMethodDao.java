package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.DeliveryMethodBeans;

public class DeliveryMethodDao {
		public DeliveryMethodBeans findById(int id) {
			Connection conn = null;

			try {
				conn = DBManager.getConnection();
				String sql = "SELECT * FROM delivery_method WHERE id =?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setInt(1, id);

				ResultSet rs = pStmt.executeQuery();

				if (!rs.next()) {
					return null;
				}

				DeliveryMethodBeans deli = new DeliveryMethodBeans();

				deli.setId(rs.getInt("id"));
				deli.setDeliveryMethodName(rs.getString("name"));
				deli.setDeliveryMethodPrice(rs.getInt("price"));

				return deli;

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}

}
