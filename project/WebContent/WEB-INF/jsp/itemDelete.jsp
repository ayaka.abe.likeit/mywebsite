<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>商品削除画面</title>
　　</head>

<body>
    <!--header-->
     <header>
        <nav class="navbar navbar-dark" style = "background-color: #98fb98;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

            <div class="nav-link ml-auto mr-5">${userInfo.name}さん</div>
              <a class="mr-5" href="UserListServlet"><img src="img/userlist.png" alt="user"></a>
                <a class="mr-5" href="ItemListServlet"><img src="img/book.png"  alt="item"></a>
                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
        </nav>
    </header>

    <div class="container">
        <div class="title-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h1>商品マスタから削除</h1>
        </div>
        </div>
    </div>

        <div class="container">
        <div class="text-center mt-5">
                <h5>${item.itemId}・${item.itemName}を本当に削除しますか</h5>
        </div>
    </div>
         <form   method="post" action="ItemDeleteServlet">
            <div class="row justify-content-md-center mt-5">

                <div class="col-md-2 mr-4">
                    <a href="ItemListServlet"><button type="button" class="btn btn-light btn-block">キャンセル</button></a>
                </div>
                <div class="col-md-2 ml-4">
                     <button  name="ok"type="submit"class="btn btn-primary btn-block" value="${item.id}">OK</button>
                </div>
            </div>
  </form>
</body>
</html>
