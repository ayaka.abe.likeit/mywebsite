package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemUpdateServlet
 */
@WebServlet("/ItemUpdateServlet")
public class ItemUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインしていない場合ログイン画面に遷移
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if(userInfo == null) {
		response.sendRedirect("LoginServlet");
		return;
		}

		if(!(userInfo.getLoginId().equals("admin")) ) {
			response.sendRedirect("HomeServlet");
			return;
		}


		//URLからパラメータを取得
		String id = request.getParameter("id");
		int itemid = Integer.parseInt(id);

		//IDから商品情報を取得しスコープにセット
		ItemDao itemDao = new ItemDao();
		ItemBeans item = itemDao.findItem(itemid);

		request.setAttribute("item", item);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String itemName = request.getParameter("itemName");
		String price = request.getParameter("price");
		String renewalDate = request.getParameter("renewalDate");
		String stock = request.getParameter("stock");
		String cost = request.getParameter("cost");
		String coment = request.getParameter("coment");

		String id = request.getParameter("id");


		//リニューアル日以外が空欄のとき
		if( itemName.isEmpty() || price.isEmpty() || stock.isEmpty() || cost.isEmpty() || coment.isEmpty()) {

			request.setAttribute("errMsg", "入力された情報は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemUpdate.jsp");
			dispatcher.forward(request, response);
			return;

		}

		//リニューアル日が空欄のときそれ以外更新
		if( renewalDate.isEmpty()) {
			ItemDao itemDao = new ItemDao();
			itemDao.ItemUpdate(itemName, price, stock, cost, coment, id);
			response.sendRedirect("ItemListServlet");
			return;
		}

		//全て更新
		ItemDao itemDao = new ItemDao();
		itemDao.ItemUpdate(itemName, price, renewalDate, stock, cost, coment, id);

		response.sendRedirect("ItemListServlet");


	}

}
