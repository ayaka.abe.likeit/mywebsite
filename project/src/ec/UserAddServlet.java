package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//フォームに入力された情報を引数にしてDaoのメソッドへ
		String loginId = request.getParameter("loginId");
		String password= request.getParameter("password");
		String passwordconfirm = request.getParameter("passwordconfirm");
		String name= request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String adress = request.getParameter("adress");


		//既に登録されているログインIDと被りがないか調べる
		//登録されたログインIDが既存のものと同じだった場合、userはnullではない

		UserDao userdao = new UserDao();
		UserBeans user = userdao.findByLoginId(loginId);

		if(user !=null ||  !(password.equals(passwordconfirm)) ||  loginId.isEmpty() || password.isEmpty() || name.isEmpty() || birthDate.isEmpty() ) {
			request.setAttribute("errMsg", "入力された情報は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);

			return;
		}

		//入力が正しい場合、インサートしてログイン画面へ
		userdao.UserInsert(loginId, password, name, birthDate, adress);
		response.sendRedirect("LoginServlet");
	}

}
