package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class AdminUserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインしていない場合ログイン画面に遷移
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if(userInfo == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		//URLのidデータを取得
		String id = request.getParameter("id");
		int userid = Integer.parseInt(id);

		//個人のデータを取り出す
		UserDao userdao = new UserDao();
		UserBeans person = userdao.findPerson(userid);

		System.out.println(id);

		request.setAttribute("person", person);

		 //管理者の場合
		if(userInfo != null && userInfo.getLoginId().equals("admin")) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminUserDetail.jsp");
			dispatcher.forward(request, response);
			return;
		}
		//一般ユーザーの場合・個人の詳細画面へ
		if(userInfo != null && !(userInfo.getLoginId().equals("admin")) ) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
			dispatcher.forward(request, response);
			return;
		}

	}

}
