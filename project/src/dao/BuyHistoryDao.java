package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import base.DBManager;
import beans.BuyHistoryBeans;

public class BuyHistoryDao {
	public  int insertBuy(BuyHistoryBeans buyData){
		Connection conn = null;
		int autoNum = -1 ;

		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO buy_history(user_id, total_price, discount_price, delivery_method_id, buy_date) VALUES(?, ?, ?, ?, NOW())";

			PreparedStatement pStmt = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);

			pStmt.setInt(1, buyData.getUserId());
			pStmt.setInt(2, buyData.getTotalPrice());
			pStmt.setInt(3, buyData.getDiscountPrice());
			pStmt.setInt(4, buyData.getDeliveryMethodId());

			pStmt.executeUpdate();

			ResultSet rs = pStmt.getGeneratedKeys();
			while(rs.next()) {
				autoNum = rs.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return autoNum;
	}

	//ユーザ情報を繰り返し出力
	public  ArrayList<BuyHistoryBeans> getUserBuyData(int userId){
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM buy_history"
							+ " JOIN delivery_method"
							+ " ON buy_history.delivery_method_id = delivery_method.id"
							+ " WHERE buy_history.user_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, userId);


			ArrayList<BuyHistoryBeans> buyList = new ArrayList<BuyHistoryBeans>();
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {

				BuyHistoryBeans buyData = new BuyHistoryBeans();

				buyData.setId(rs.getInt("id"));
				buyData.setTotalPrice(rs.getInt("total_price"));
				buyData.setDeliveryMethodId(rs.getInt("delivery_method_id"));
				buyData.setDeliveryMethodPrice(rs.getInt("price"));
				buyData.setDeliveryMethodName(rs.getString("name"));
				buyData.setBuyDate(rs.getTimestamp("buy_date"));

				buyList.add(buyData);

			}

			return buyList;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

}
