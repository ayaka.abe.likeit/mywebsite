package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DeliveryMethodBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.DeliveryMethodDao;
import dao.UserDao;

/**
 * Servlet implementation class BuyServlet
 */
@WebServlet("/BuyServlet")
public class BuyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインした人の情報をセッションから取得
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		//ログインしていなかった場合ログインしてもらう
		if(userInfo == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//ログインIDから個人の情報を取得
		String loginId = userInfo.getLoginId();

		UserDao userDao =new UserDao();
		UserBeans user = userDao.findbyLoginId(loginId) ;

		//取得した情報をセット
		request.setAttribute("user", user);


		//カートの情報を取得
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		//カートに商品が入っていない場合、ひとつ前の画面に戻す
		if(cart.size() == 0) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);
			return;
		}


		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buy.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//カートのセッション確認
		HttpSession session = request.getSession();
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		//選んだ配送方法を受け取る
		String id = request.getParameter("deliveryName");
		int deliId = Integer.parseInt(id);

		//選んだ配送方法を呼び出す
		DeliveryMethodDao deliDao = new DeliveryMethodDao();
		DeliveryMethodBeans deli = deliDao.findById(deliId);

		//カート情報更新
		session.setAttribute("deli", deli);

		response.sendRedirect("BuyConfirmServlet");
	}

}
