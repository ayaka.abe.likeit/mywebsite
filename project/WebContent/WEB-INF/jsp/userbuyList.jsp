<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>注文履歴画面</title>
</head>

<body>
   <!--header-->
     <header>
        <nav class="navbar navbar-dark" style = "background-color: #ffc3c3;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>
	            	<div class="nav-link ml-auto mr-5"><a href="UserBuyListServlet?id=${user.id}">${userInfo.name}さん</a></div>
	            	<a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
	                <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
	                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
        </nav>
    </header>

    <div class="container">
        <div class="text-center">
            <div class="title-margin">
                <h2>注文履歴一覧</h2>
            </div>
         </div>
    </div>

    <div class="container">
        <div class="row justify-content-md-center">
             <label for="name" class="col-md-4 "><h5>${user.name}さま</h5></label>
             <a href ="UserDetailServlet?id=${user.id}"><button type="button" class="btn btn-light mr-5">ユーザ情報詳細</button></a>
             <a href ="UserUpdateServlet?id=${user.id}"><button type="button" class="btn btn-outline-secondary">ユーザ情報更新</button></a>
        </div>

    </div>


    <div class="container title-margin">
        <table class="table"  id="table">
          <thead>
            <tr>
              <th>購入日</th>
              <th>購入金額</th>
              <th>購入内容詳細</th>
            </tr>
          </thead>
          <tbody>
          <c:forEach var="buy" items="${buylist}">
            <tr>
              <td>${buy.buyFormatDate}</td>
              <td>${buy.totalPrice + buy.deliveryMethodPrice}円</td>
              <td><a href="BuyHistoryServlet?buy_id=${buy.id}"><button type="button" class="btn btn-outline-info">注文内容詳細</button></a></td>
            </tr>
            </c:forEach>
          </tbody>
        </table>
        </div>
    </body>
</html>



