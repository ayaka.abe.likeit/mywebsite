package ec;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BCategoryBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.BCategoryDao;
import dao.ItemDao;
import dao.UserDao;

/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/ItemDetailServlet")
public class ItemDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインした人の情報をセッションから取得
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		//ログインしていない人用
		if(userInfo == null) {
			//Bカテゴリーの情報を取り出す
			BCategoryDao  dao = new BCategoryDao();
			List<BCategoryBeans> catelist = dao.BcateAll();

			request.setAttribute("catelist", catelist);

			//URLからIDを取得
			String id = request.getParameter("id");
			int itemid = Integer.parseInt(id);

			//商品情報を取り出す
			ItemDao itemDao = new ItemDao();
			ItemBeans item = itemDao.findItem(itemid);

			request.setAttribute("item", item);



			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemDetail.jsp");
			dispatcher.forward(request, response);
			return;
		}


		//ログインIDから個人の情報を取得
		UserDao userDao =new UserDao();
		UserBeans user = userDao.findbyLoginId(userInfo.getLoginId());

		//取得した情報をセット
		request.setAttribute("user", user);


		//Bカテゴリーの情報を取り出す
		BCategoryDao  dao = new BCategoryDao();
		List<BCategoryBeans> catelist = dao.BcateAll();

		request.setAttribute("catelist", catelist);

		//URLからIDを取得
		String id = request.getParameter("id");
		int itemid = Integer.parseInt(id);

		//商品情報を取り出す
		ItemDao itemDao = new ItemDao();
		ItemBeans item = itemDao.findItem(itemid);

		request.setAttribute("item", item);



		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemDetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");


		//URLからIDを取得してIDに紐づく情報を取得
		String id = request.getParameter("add");
		int itemid = Integer.parseInt(id);

		ItemDao itemDao = new ItemDao();
		ItemBeans item = itemDao.findItem(itemid);

		request.setAttribute("item", item);

		//カートのセッション確認
		HttpSession session = request.getSession();
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		//カートがなければ作成
		if (cart == null) {
			cart = new ArrayList<ItemBeans>();
		}

		//カートに商品を追加
		cart.add(item);

		//カート情報更新
		session.setAttribute("cart", cart);

		response.sendRedirect("CartServlet");


	}

}
