<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="style.css" rel="stylesheet" type="text/css" />
        <title>商品検索一覧画面</title>
</head>

<body>
    <!--header-->
     <header>
        <nav class="navbar navbar-dark" style = "background-color: #98fb98;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

            <div class="nav-link ml-auto mr-5">${userInfo.name}さん</div>
              <a class="mr-5" href="UserListServlet"><img src="img/userlist.png" alt="user"></a>
                <a class="mr-5" href="ItemListServlet"><img src="img/book.png"  alt="item"></a>
                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
        </nav>
    </header>

    <div class="container">
        <div class="title-margin">
            <div class="row justify-content-md-center col-md-auto">
                <h1>商品検索一覧画面</h1>
            </div>
            <div class="itemadd-margin">
                <a href ="ItemAddServlet"><button type="button" class="btn btn-outline-info">商品マスタ新規登録</button></a>
            </div>
        </div>
    </div>

    <div class="container">
       <div class="justify-content-md-center col-md-auto">
           <form method="post" action="ItemListServlet" class="form-horizontal">
            <div class="form-group row">
                <label for="itemId" class="col-md-2 col-form-label">商品ID</label>
                <div class="col-md-8">
                <input type="text" class="form-control" id="itemId" name="itemIdP">
                </div>
            </div>

            <div class="form-group row  mt-3">
                <label for="itemName" class="col-md-2 col-form-label">商品名</label>
                <div class="col-md-8">
                <input type="text" class="form-control" id="itemName" name="itemNameP">
                </div>
            </div>

            <div class="form-group row  mt-3">
                <label for="brand" class="col-md-2 col-form-label">ブランド名</label>
                <div class="col-md-8">
                <input type="text" class="form-control" id="brand" name="brandP">
                </div>
            </div>

            <div class="form-group row mt-3">
                <label for="category" class="col-md-2  col-form-label">カテゴリー</label>
                     <div class="col-md-8">
                        <select name="categoryP" id="category">
                            <option value="">選択してください</option>
                                <optgroup label="スキンケア">
                                    <option value="1" label="化粧水">化粧水</option>
                                    <option value="2" label="美容液">美容液</option>
                                    <option value="3" label="乳液">乳液</option>
                                    <option value="4" label="フェイスパック">フェイスパック</option>
                                </optgroup>
                            <optgroup label="UVケア">
                                <option value="5" label="日焼け止め">日焼け止め</option>
                            </optgroup>
                            <optgroup label="ベースメイク">
                                <option value="6" label="化粧下地">化粧下地</option>
                                <option value="7" label="ファンデーション">ファンデーション</option>
                                <option value="8" label="コンシーラー">コンシーラー</option>
                            </optgroup>
                            <optgroup label="メイクアップ">
                                <option value="9" label="口紅・グロス">口紅・グロス</option>
                                <option value="10" label="チーク">チーク</option>
                                <option value="11" label="アイブロウ">アイブロウ</option>
                            </optgroup>
                            <optgroup label="香水">
                                <option value="12" label="香水">香水</option>
                            </optgroup>
                        </select>
                   </div>
            </div>

            <div class="form-group row  mt-3">
                <label for="saleDate" class="col-md-2 col-form-label">発売日</label>

                <div class="row col-md-10">
                    <div class="col-md-4">
                        <input type="date" class="form-control" id="saleDate" name="saleDate1P">
                    </div>

                    <div class="col-md-2 text-center">
                        <h5>～</h5>
                    </div>

                    <div class="col-md-4">
                        <input type="date" class="form-control" id="saledate" name="saleDate2P">
                    </div>
                </div>
            </div>


          <div class="btn-margin2">
                <button type="submit" class="btn btn-primary">検索</button>
          </div>
         </form>
       </div>


        <table class="table mt-5" id="table">
            <thead>
                <tr>
                <th scope="col">商品ID</th>
                <th scope="col">商品名</th>
                <th scope="col">ブランド名</th>
                <th scope="col">発売日</th>
                <th scope="col"></th>
                </tr>
            </thead>
        <tbody>

			<c:forEach var="item" items="${itemlist}">
                <tr>
                  <th scope="row">${item.itemId}</th>
                  <td>${item.itemName}</td>
                  <td>${item.brand}</td>
                  <td>${item.saleDate}</td>
                    <td> <a href="MasterDetailServlet?id=${item.id}"><button type="button" class="btn btn-primary">詳細</button></a>
                        <a href="ItemUpdateServlet?id=${item.id}"><button type="button" class="btn btn-success">更新</button></a>
                        <a href="ItemDeleteServlet?id=${item.id}"><button type="button" class="btn btn-danger">削除</button></a>
                  </td>
                </tr>
			</c:forEach>
        </tbody>
        </table>
    </div>
    </body>
</html>