package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyHistoryBeans;
import beans.DeliveryMethodBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class BuyConfirmServlet
 */
@WebServlet("/BuyConfirmServlet")
public class BuyConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyConfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインした人の情報をセッションから取得
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		UserDao userDao =new UserDao();
		UserBeans user = userDao.findbyLoginId(userInfo.getLoginId());

		//取得した情報をセット
		request.setAttribute("user", user);

		//カートの情報を取得
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		//配送情報を取得
		DeliveryMethodBeans deli = (DeliveryMethodBeans)session.getAttribute("deli");

		//商品の合計金額
		int totalPrice = ItemBeans.getTotalItemPrice(cart);
		int discountPrice = 0;

		if(cart.size() >= 3) {
			discountPrice = (int) (totalPrice * 0.9);
		}

		BuyHistoryBeans buyData = new BuyHistoryBeans();

		buyData.setUserId(user.getId());
		buyData.setTotalPrice(totalPrice);
		buyData.setDiscountPrice(discountPrice);
		buyData.setDeliveryMethodId(deli.getId());
		buyData.setDeliveryMethodName(deli.getDeliveryMethodName());
		buyData.setDeliveryMethodPrice(deli.getDeliveryMethodPrice());

		session.setAttribute("buyData", buyData);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyConfirm.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

}
