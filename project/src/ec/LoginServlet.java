package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//セッションがあった場合・以下分岐
		HttpSession session = request.getSession();
		 UserBeans user = (UserBeans) session.getAttribute("userInfo");

		 //管理者の場合・ユーザ一覧へ
		if(user != null && user.getLoginId().equals("admin")) {
			response.sendRedirect("UserListServlet");
			return;
		}
		//一般ユーザーの場合・ホーム画面へ
		if(user != null && !(user.getLoginId().equals("admin")) ) {
			response.sendRedirect("HomeServlet");
			return;
		}

		//セッションがない、通常のログイン時
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);

	}



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");
		//
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		UserDao userdao = new UserDao();
		UserBeans user = userdao.findByLoginInfo(loginId, password);


		//ユーザーが見つからなかったとき
		if(user == null) {
			request.setAttribute("errMsg", "ログインに失敗しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//セッションにユーザ情報をセットする
		HttpSession session = request.getSession();
		session.setAttribute("userInfo", user);

		//ログインした人が管理者の場合
		if(user.getLoginId().equals("admin")){
			response.sendRedirect("UserListServlet");
			return;
		}

		//一般ユーザー
		response.sendRedirect("HomeServlet");



	}

}