package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class CartServlet
 */
@WebServlet("/CartServlet")
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインした人の情報をセッションから取得
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		//ログインしていない人用
		if(userInfo == null) {
			//カートの情報を取得
			ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

			//カートがない場合カートを作成
			if (cart == null) {
				cart = new ArrayList<ItemBeans>();
				session.setAttribute("cart", cart);
			}

			//カートに商品が入っていない場合
			if(cart.size() == 0) {
				request.setAttribute("cartMsg", "カートには何も入っていません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
				dispatcher.forward(request, response);
				return;
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);
		}

		//ログインIDから個人の情報を取得
		String loginId = userInfo.getLoginId();

		UserDao userDao =new UserDao();
		UserBeans user = userDao.findbyLoginId(loginId) ;

		//取得した情報をセット
		request.setAttribute("user", user);


		//カートの情報を取得
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		//カートがない場合カートを作成
		if (cart == null) {
			cart = new ArrayList<ItemBeans>();
			session.setAttribute("cart", cart);
		}

		//カートに商品が入っていない場合
		if(cart.size() == 0) {
			request.setAttribute("cartMsg", "カートには何も入っていません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
			dispatcher.forward(request, response);
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//ログインした人の情報をセッションから取得
		HttpSession session = request.getSession();

		//ParameterValuesで配列に入れる
		String[] deleteItemList = request.getParameterValues("delete");

		//カートの情報を取得
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");


		if (deleteItemList != null) {
			//削除対象の商品を配列に入れていく
			for (String deleteItemId : deleteItemList) {
				//カートの中身を繰り返す
				for (ItemBeans cartItem : cart) {
					//カートの中身と削除リストに入っているもののIDが一致したときにArrayListからremove
					if (cartItem.getId() == Integer.parseInt(deleteItemId)) {
						cart.remove(cartItem);
						break;
					}
				}
			}
		}


		response.sendRedirect("CartServlet");
	}

}
