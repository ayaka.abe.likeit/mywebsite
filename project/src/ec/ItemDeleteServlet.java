package ec;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemDeleteServlet
 */
@WebServlet("/ItemDeleteServlet")
public class ItemDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインしていない場合ログイン画面に遷移
		HttpSession session = request.getSession();
		UserBeans userInfo = (UserBeans) session.getAttribute("userInfo");

		if(userInfo == null) {
		response.sendRedirect("LoginServlet");
		return;
		}

		if(!(userInfo.getLoginId().equals("admin")) ) {
			response.sendRedirect("HomeServlet");
			return;
		}


		//URLからパラメータを取得
		String id = request.getParameter("id");
		int itemid = Integer.parseInt(id);

		//IDから商品情報を取得しスコープにセット
		ItemDao itemDao = new ItemDao();
		ItemBeans item = itemDao.findItem(itemid);

		request.setAttribute("item", item);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemDelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//nameにok,valueにidを指定してパラメータを渡す
		String id = request.getParameter("ok");

		//削除実行
		ItemDao itemDao = new ItemDao();
		itemDao.ItemDelete(Integer.parseInt(id));

		response.sendRedirect("ItemListServlet");
	}

}
