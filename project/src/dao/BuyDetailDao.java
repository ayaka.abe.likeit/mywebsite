package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.BuyHistoryBeans;
import beans.ItemBeans;

public class BuyDetailDao {
	public static void insertBuyDetail(int buyId, int itemId){
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO buy_detail(buy_id , item_id) VALUES( ?, ? )";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, buyId);
			pStmt.setInt(2, itemId);

			pStmt.executeUpdate();

		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public  BuyHistoryBeans getUserBuyDetail(int buyId)  {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM buy_detail"
					+ " JOIN buy_history"
					+ " ON  buy_detail.buy_id = buy_history.id"
					+ " JOIN delivery_method"
					+ " ON  buy_history.delivery_method_id = delivery_method.id"
					+ " WHERE buy_detail.buy_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, buyId);

			ResultSet rs = pStmt.executeQuery();


			BuyHistoryBeans buyhisData = new BuyHistoryBeans();

			if (rs.next()) {
				buyhisData.setId(rs.getInt("id"));
				buyhisData.setUserId(rs.getInt("user_id"));
				buyhisData.setTotalPrice(rs.getInt("total_price"));
				buyhisData.setDiscountPrice(rs.getInt("discount_price"));
				buyhisData.setDeliveryMethodId(rs.getInt("delivery_method_id"));
				buyhisData.setDeliveryMethodPrice(rs.getInt("price"));
				buyhisData.setDeliveryMethodName(rs.getString("name"));
				buyhisData.setBuyDate(rs.getTimestamp("buy_date"));
			}

			return buyhisData;

		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}
	public  ArrayList<ItemBeans> getItemBuyListByBuyId(int buyId){
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM item"
					+ " JOIN buy_detail"
					+ " ON item.id = buy_detail.item_id"
					+ " WHERE buy_detail.buy_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, buyId);

			ResultSet rs = pStmt.executeQuery();
			ArrayList<ItemBeans> buyItemList = new ArrayList<ItemBeans>();

			while (rs.next()) {
				ItemBeans itemData = new ItemBeans();
				itemData.setId(rs.getInt("item.id"));
				itemData.setItemName(rs.getString("item_name"));
				itemData.setSellPrice(rs.getInt("sell_price"));

				buyItemList.add(itemData);
			}

			return buyItemList;

		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}
}
