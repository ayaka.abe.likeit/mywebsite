package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserBeans;

public class UserDao {

//ログイン時に登録されているIDとパスワードがあるか確認
	public UserBeans findByLoginInfo(String loginId, String password) {
		Connection conn = null;

		try {
		conn = DBManager.getConnection();

		String sql = "SELECT*FROM user WHERE login_id = ? and login_password = ?";
		PreparedStatement pStmt = conn.prepareStatement(sql);

		String md5 = MD5(password);

		pStmt.setString(1, loginId);
		pStmt.setString(2, md5);

		ResultSet rs = pStmt.executeQuery();

		if(!rs.next()) {
			return null;
		}
		String loginIdData = rs.getString("login_id");
		String nameData = rs.getString("user_name");

		return new UserBeans(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

//新規登録時に重複したログインIDがないか確認
		public UserBeans findByLoginId(String loginId) {
			Connection conn = null;

			try {
			conn = DBManager.getConnection();

			String sql = "SELECT*FROM user WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, loginId);


			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}
			String loginIdData = rs.getString("login_id");


			return new UserBeans(loginIdData);

			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}
		}


//新規登録時に使用
	public void UserInsert(String loginId, String password, String name, String birthDate, String adress) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "INSERT INTO user(login_id, login_password, user_name, birth_date, adress, create_date, update_date)Values(? ,? ,? ,? ,? ,NOW() ,NOW() )";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			String md5 = MD5(password);

			pStmt.setString(1, loginId);
			pStmt.setString(2, md5);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);
			pStmt.setString(5, adress);

			pStmt.executeUpdate();


		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

//アップデート
	public void Update(String password, String name, String birthDate, String adress, String id ) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "UPDATE user SET login_password = ?, user_name = ?, birth_date = ?, adress = ?, update_date = NOW()  WHERE id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			String md5 = MD5(password);

			pStmt.setString(1, password);
			pStmt.setString(2, md5);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, adress);
			pStmt.setString(5, id);

			pStmt.executeUpdate();

		}catch (SQLException e) {
			e.printStackTrace();
			} finally {
			 // データベース切断
			          if (conn != null) {
			          	try {
			               conn.close();
			            } catch (SQLException e) {
			                	e.printStackTrace();
			            }
			 }
			}
	}

//アップデート・パスワード以外
	public void Update(String name, String birthDate, String adress, String id ) {
		Connection conn = null;

			try {
				conn = DBManager.getConnection();
				String sql = "UPDATE user SET user_name = ?, birth_date = ?, adress = ?, update_date = NOW()  WHERE id =?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, name);
				pStmt.setString(2, birthDate);
				pStmt.setString(3, adress);
				pStmt.setString(4, id);

				pStmt.executeUpdate();

			}catch (SQLException e) {
				e.printStackTrace();
				} finally {
				 // データベース切断
				          if (conn != null) {
				          	try {
				               conn.close();
				            } catch (SQLException e) {
				                	e.printStackTrace();
				            }
				          }
				}
		}


//ユーザーデータ削除
	 public void Delete(String id) {
			Connection conn = null;

			try {
				conn = DBManager.getConnection();

				String sql = "DELETE FROM user WHERE id = ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);

				pStmt.setString(1, id);

				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();

			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}

		}

//ユーザー検索
	 public List<UserBeans>findSearch(String loginIdP, String nameP, String birthDate1P, String birthDate2P){
		 Connection conn = null;
		 List<UserBeans> userlist = new ArrayList<UserBeans>();

		 try {
				conn = DBManager.getConnection();
				String sql = "SELECT * FROM user WHERE id !=1";

				if(!loginIdP.equals("")) {
					 sql += " AND login_id = '" + loginIdP + "'";
				}

				if(!nameP.equals("")) {
					 sql += " AND name LIKE '%" + nameP+ "%'";
				}

				if(!birthDate1P.equals("")) {
					 sql += " AND birth_date >= '" + birthDate1P + "'";
				}

				if(!birthDate2P.equals("")) {
					 sql += " AND birth_date <= '" + birthDate2P + "'";
				}

				Statement stmt = conn.createStatement();
		        ResultSet rs = stmt.executeQuery(sql);

				while(rs.next()){

					int id = rs.getInt("id");
			    	String loginId = rs.getString("login_id");
			    	String password = rs.getString("login_password");
			        String name = rs.getString("user_name");
			        Date birthDate = rs.getDate("birth_date");
			        String adress = rs.getString("adress");
			        Date createDate = rs.getDate("create_date");
			        Date updateDate = rs.getDate("update_date");

			        UserBeans user = new UserBeans(id, loginId, password, name, birthDate, adress, createDate, updateDate);
			        userlist.add(user);
				}


			}catch(SQLException e){
				e.printStackTrace();
	        	return null;
			} finally {
				 // データベース切断
		          if (conn != null) {
		          	try {
		               conn.close();
		            } catch (SQLException e) {
		                	e.printStackTrace();

		            }
		     }
		}
			//戻り値を返す
			return userlist;
	 }





//全ユーザーの全ての情報を取り出す
	public List<UserBeans> findAll(){
		Connection conn = null;
		List<UserBeans> userlist = new ArrayList<UserBeans>();

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id !=1";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()){

				int id = rs.getInt("id");
		    	String loginId = rs.getString("login_id");
		    	String password = rs.getString("login_password");
		        String name = rs.getString("user_name");
		        Date birthDate = rs.getDate("birth_date");
		        String adress = rs.getString("adress");
		        Date createDate = rs.getDate("create_date");
		        Date updateDate = rs.getDate("update_date");

		        //userに取り出したものを詰めてuserlistに加える
		        UserBeans user = new UserBeans(id, loginId, password, name, birthDate, adress, createDate, updateDate);
		        userlist.add(user);
			}


		}catch(SQLException e){
			e.printStackTrace();
        	return null;
		} finally {
			 // データベース切断
	          if (conn != null) {
	          	try {
	               conn.close();
	            } catch (SQLException e) {
	                	e.printStackTrace();

	            }
	     }
	}
		//戻り値を返す
		return userlist;
	}

//ユーザー詳細情報を1人分取り出す
	public UserBeans findPerson(int id) {
		Connection conn = null;

				try {
					conn = DBManager.getConnection();
					String sql = "SELECT * FROM user WHERE id =?";

					PreparedStatement pStmt = conn.prepareStatement(sql);
					pStmt.setInt(1,id);

					ResultSet rs = pStmt.executeQuery();

					if(!rs.next()){
						return null;
					}

					UserBeans person = new UserBeans();

					person.setId(rs.getInt("id"));
					person.setLoginId(rs.getString("login_id"));
				    person.setPassword(rs.getString("login_password"));
				    person.setName(rs.getString("user_name"));
				    person.setBirthDate(rs.getDate("birth_date"));
				    person.setAdress( rs.getString("adress"));
				    person.setCreateDate(rs.getTimestamp("create_date"));
				    person.setUpdateDate( rs.getTimestamp("update_date"));

				    return person ;


				}catch (SQLException e) {
				e.printStackTrace();
				return null;
				} finally {
				 // データベース切断
				          if (conn != null) {
				          	try {
				               conn.close();
				            } catch (SQLException e) {
				                	e.printStackTrace();
				            }
				 }
				}
			}

//ユーザー詳細情報を1人分取り出す
		public UserBeans findbyLoginId(String loginId) {
			Connection conn = null;

					try {
						conn = DBManager.getConnection();
						String sql = "SELECT * FROM user WHERE login_id =?";

						PreparedStatement pStmt = conn.prepareStatement(sql);
						pStmt.setString(1,loginId);

						ResultSet rs = pStmt.executeQuery();

						if(!rs.next()){
							return null;
						}

						UserBeans person = new UserBeans();

						person.setId(rs.getInt("id"));
						person.setLoginId(rs.getString("login_id"));
					    person.setPassword(rs.getString("login_password"));
					    person.setName(rs.getString("user_name"));
					    person.setBirthDate(rs.getDate("birth_date"));
					    person.setAdress( rs.getString("adress"));
					    person.setCreateDate(rs.getTimestamp("create_date"));
					    person.setUpdateDate( rs.getTimestamp("update_date"));

					    return person ;


					}catch (SQLException e) {
					e.printStackTrace();
					return null;
					} finally {
					 // データベース切断
					          if (conn != null) {
					          	try {
					               conn.close();
					            } catch (SQLException e) {
					                	e.printStackTrace();
					            }
					 }
					}
				}
//MD5で暗号化
	 public static String MD5(String password) {

		 String source = password;

		 Charset charset = StandardCharsets.UTF_8;

		 String algorithm = "MD5";

		 byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		 String result = DatatypeConverter.printHexBinary(bytes);
		 System.out.println(result);

		 return result;
	 }



}
