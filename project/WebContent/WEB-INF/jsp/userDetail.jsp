<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html  lang="ja">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
         <link href="style.css" rel="stylesheet" type="text/css" />
    <title>ユーザ詳細</title>
    </head>

<body>
     <header>
        <nav class="navbar navbar-dark" style = "background-color: #ffc3c3;">
            <a class="navbar-brand" href="HomeServlet"><h3>shop名</h3></a>

            <div class="nav-link ml-auto mr-5"><a href="UserBuyListServlet?id=${person.id}">${userInfo.name}さん</a></div>
            	 <a class="mr-5" href="CartServlet"><img src="img/shopping-cart-icon-free56.png"  alt="cart"></a>
                <a class="mr-5" href ="UserAddServlet"><button type="button" class="btn btn-info btn-lg">新規登録</button></a>
                <a href ="LogoutServlet"><button type="button" class="btn btn-info btn-lg">ログアウト</button></a>
        </nav>
       </header>

    <div class="container">
        <div class="title-margin">
        <div class="row justify-content-md-center col-md-auto">
            <h1>ユーザ情報詳細</h1>
        </div>
        </div>
    </div>


    <div class="container user-left">
        <div class="form-group row mt-3">
            <label for="loginid" class="col-md-2 col-form-label">ログインID</label>
            <div class="col-md-auto">
                <div class="form-control-plaintext">${person.loginId}
                </div>
            </div>
        </div>


        <div class="form-group row mt-3">
            <label for="name" class="col-md-2 col-form-label">ユーザ名</label>
             <div class="col-md-auto">
                <div class="form-control-plaintext">${person.name}
                </div>
            </div>
        </div>

        <div class="form-group row mt-3">
            <label for="birthdate" class="col-md-2 col-form-label">生年月日</label>
             <div class="col-md-auto">
                <div class="form-control-plaintext">${person.birthDate}
                </div>
            </div>
        </div>

         <div class="form-group row mt-3">
            <label for="adress" class="col-md-2 col-form-label">住所</label>
            <div class="col-md-auto">
                <div class="form-control-plaintext">${person.adress}
                </div>
            </div>
        </div>

        <div class="form-group row mt-3">
            <label for="add-date" class="col-md-2 col-form-label">登録日時</label>
            <div class="col-md-auto">
                <div class="form-control-plaintext">${person.createFormatDate}
                </div>
            </div>
        </div>

        <div class="form-group row  mt-3">
            <label for="update-date" class="col-md-2  col-form-label">更新日時</label>
             <div class="col-md-auto">
                <div class="form-control-plaintext">${person.updateFormatDate}
                </div>
            </div>
        </div>
    </div>

        <div class="row justify-content-md-center mt-5">
            <a href="UserBuyListServlet"><u> 戻る </u></a>
        </div>

</body>
</html>